from distutils.core import setup

setup(
    name='kls-scraper',
    version='1.0.0',
    py_modules=['kls_scraper'],
    url='',
    license='',
    author='Rhistina Revilla',
    author_email='rhistina@gmail.com',
    description="A Python 3.5 program that visits KLS's website and scrapes for the phone number.",
)
