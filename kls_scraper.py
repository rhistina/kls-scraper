from bs4 import BeautifulSoup as bs, NavigableString, Tag
import requests as r

"""
    make_request - simple request to URL
"""
def make_request(url):
    print ("Visiting {}".format(url))
    return r.get(url)

"""
    find_contact_page - Scrapes main URL for a hyperlink for "Contact Us"
"""
def find_contact_page(url):
    response = make_request(url)

    if response.ok:
        soup = bs(response.content, "lxml")

        link  = soup.find('a', href=True, text="Contact Us")

        contact_page_url = url + link.get('href')

        return contact_page_url
    else:
        print ("Unable to make request to {}".format(url))
        return

"""
    find_contact_page - Given the contact page, scrapes for the number between line breaks on page
"""
def find_number_on_contact_page(url):
    response = make_request(url)

    if response.ok:
        soup = bs(response.content, "lxml")

        for br in soup.find_all('br'):
            next = br.next_sibling
            if not (next and isinstance(next,NavigableString)):
                continue
            next2 = next.next_sibling

            if next2 and isinstance(next2, Tag) and next2.name == 'br':
                text = str(next).strip()
                if text[0] == "T":
                    next = next.replace(u'T\xa0', u'').strip()
                    return next
    else:
        print ("Unable to make request to {}".format(url))
        return

if __name__ == "__main__":
    main_url = "https://klsdiversified.com"
    contact_url = find_contact_page(main_url)
    contact_number = find_number_on_contact_page(contact_url)
    if contact_number:
        print ("Contact KLS Diversified @ {}".format(contact_number))
    else:
        print ("Unable to find contact number from {}".format(contact_url))
