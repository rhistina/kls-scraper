# KLS-SCRAPER #

### Description ###

A Python 3.5 program that visits [KLS's website](http://www.klsdiversified.com) and scrapes for the phone number.

### Installation ###

#### Via Docker Image ####


```
#!python

sudo docker build -t rhistina-kls-scraper-zoo .
sudo docker run -t rhistina-kls-scraper-zoo
sudo docker exec -it <container id> bash

```

#### Via irtual environment ####
```
#!python

pip install -r requirements.txt
```

### Usage ###

After install dependencies, run below to execute:
```
#!python

python kls_scraper.py 
```

Expected Output:

```
#!python
>>> python kls_scraper.py
Visiting https://klsdiversified.com
Visiting https://klsdiversified.com/contact.pl
Contact KLS Diversified @ 212.905.0800
```




### Contact ###

* Rhistina Revilla (rhistina@gmail.com)